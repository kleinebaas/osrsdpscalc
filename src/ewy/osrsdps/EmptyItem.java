package ewy.osrsdps;

class EmptyItem extends Item {

    EmptyItem() {
        this.stats = new int[14];
        this.name = "None";
        type = Keywords.Types.MELEE;
    }
}
