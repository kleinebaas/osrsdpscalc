package ewy.osrsdps.util;

public class NamedString {

    public String name;
    public String value;

    public NamedString(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
