package ewy.osrsdps;

public class Item {
    int[] stats;
    String name;
    Keywords.Types type;

    public Item() {
    }

    public Item(String name, Object[] stats) {
        int[] array = new int[stats.length];
        for (int i = 0; i < stats.length; i++) {
            if (stats[i] instanceof Integer) {
                array[i] = (int) (stats[i]);
            } else {
                array[i] = 0;
            }
        }
        this.stats = array;
        this.name = name;
        if (array[Keywords.rangedAttack] > 0) {
            type = Keywords.Types.RANGED;
        } else if (array[Keywords.magicAttack] > 0) {
            type = Keywords.Types.MAGIC;
        } else {
            type = Keywords.Types.MELEE;
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
