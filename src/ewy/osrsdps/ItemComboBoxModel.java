package ewy.osrsdps;

import javax.swing.*;
import java.util.Comparator;

/*
 *  Custom model to make sure the items are stored in a sorted order.
 *  The default is to sort in the natural order of the item, but a
 *  Comparator can be used to customize the sort order.
 */
//class SortedComboBoxModel extends DefaultComboBoxModel
class ItemComboBoxModel extends DefaultComboBoxModel<Item> {
    private Comparator comparator;

    public ItemComboBoxModel() {
        super();
    }

    public void addElement(EmptyItem item) {
        super.insertElementAt(item, 0);
    }

    @Override
    public void addElement(Item element) {
        insertElementAt(element, 0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void insertElementAt(Item element, int index) {
        int size = getSize();

        //  Determine where to insert element to keep model in sorted order

        for (index = 0; index < size; index++) {
            if (comparator != null) {
                Item o = getElementAt(index);

                if (comparator.compare(o, element) > 0)
                    break;
            } else {
                Comparable c = getElementAt(index).name;

                if (c.compareTo(element.name) > 0)
                    break;
            }
        }

        super.insertElementAt(element, index);

        //  Select an element when it is added to the beginning of the model

        if (index == 0 && element != null) {
            setSelectedItem(element);
        }
    }
}
