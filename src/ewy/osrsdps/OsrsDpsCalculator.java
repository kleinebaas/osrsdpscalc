package ewy.osrsdps;

import ewy.osrsdps.http.HighscoresRetreiver;
import ewy.osrsdps.http.OsrsDatabaseGenerator;
import ewy.osrsdps.util.NamedString;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Objects;

public class OsrsDpsCalculator extends JFrame {
    public static File dbDir = new File(System.getProperty("user.home") + "/.ewy/dps");
    private JPanel contentPanel;
    private JPanel rsnFrame;
    private JTextField rsnField;
    private JButton fetchLevelsButton;
    private JPanel levelsPanel;
    private JPanel allyPanel;
    private JSpinner attackSpinner;
    private JPanel skillPanel;
    private JPanel spinnerPanel;
    private JPanel prayerPanel;
    private JPanel boostsPanel;
    private JPanel equipmentPanel;
    private JComboBox<Item> ringComboBox;
    private JPanel equipmentLabelPanel;
    private JPanel equipmentSelectionPanel;
    private JCheckBox twoHandedCheckBox;
    private JComboBox<Item> weaponComboBox;
    private JComboBox<Item> twohComboBox;
    private JComboBox combatComboBox;
    private JComboBox spellComboBox;
    private JButton fetchItemsButton;
    private JComboBox<Item> ammoComboBox;
    private JComboBox<Item> headComboBox;
    private JComboBox<Item> capeComboBox;
    private JComboBox<Item> amuletComboBox;
    private JComboBox<Item> chestComboBox;
    private JComboBox<Item> legsComboBox;
    private JComboBox<Item> shieldComboBox;
    private JComboBox<Item> glovesComboBox;
    private JComboBox<Item> bootsComboBox;
    private JLabel quickTest;
    private JSpinner strengthSpinner;
    private JSpinner defenceSpinner;
    private JSpinner rangedSpinner;
    private JSpinner magicSpinner;
    private JLabel maxHit;
    private JComboBox attackPrayer;
    private JComboBox strengthPrayer;
    private JComboBox defencePrayer;
    private JComboBox rangedPrayer;
    private JComboBox magicPrayer;
    private JLabel dpsLabel;
    private JPanel resultsPanel;
    private JLabel speedLabel;
    private JPanel enemyPanel;
    private JSpinner prayerSpinner;
    private JSpinner hitpointsSpinner;
    private JSpinner spinner1;
    private JSpinner spinner2;
    private JSpinner spinner3;
    private JComboBox comboBox1;

    private OsrsDpsCalculator(String[] args) {
        super("EwySoft DPS Calculator");
        setContentPane(contentPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            fillComboBoxes();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        iterateThroughComponents(contentPanel);

        for (Component comp : spinnerPanel.getComponents()) {
            if (comp instanceof JSpinner) {
                JSpinner spinner = (JSpinner) comp;
                SpinnerNumberModel model = new SpinnerNumberModel();
                model.setMaximum(99);
                model.setMinimum(1);
                spinner.setModel(model);
                spinner.setValue(1);
            }
        }
        twoHandedCheckBox.addActionListener(e -> {
            if (twoHandedCheckBox.isSelected()) {
                weaponComboBox.setEnabled(false);
                shieldComboBox.setEnabled(false);
                twohComboBox.setEnabled(true);
            } else {
                weaponComboBox.setEnabled(true);
                shieldComboBox.setEnabled(true);
                twohComboBox.setEnabled(false);
            }
        });

        ActionListener specialPrayerListener = e -> {
            JComboBox source = (JComboBox) e.getSource();
            if (Objects.equals(source.getSelectedItem(), "Chivalry") || Objects.equals(source.getSelectedItem(), "Piety")) {
                attackPrayer.setSelectedItem(source.getSelectedItem());
                strengthPrayer.setSelectedItem(source.getSelectedItem());
                defencePrayer.setSelectedItem(source.getSelectedItem());
            } else {
                if (Objects.equals(attackPrayer.getSelectedItem(), "Chivalry") || Objects.equals(attackPrayer.getSelectedItem(), "Piety") ||
                        Objects.equals(strengthPrayer.getSelectedItem(), "Chivalry") || Objects.equals(strengthPrayer.getSelectedItem(), "Piety") ||
                        Objects.equals(defencePrayer.getSelectedItem(), "Chivalry") || Objects.equals(defencePrayer.getSelectedItem(), "Piety")) {
                    if (attackPrayer != e.getSource()) {
                        attackPrayer.setSelectedItem("None");
                    }
                    if (strengthPrayer != e.getSource()) {
                        strengthPrayer.setSelectedItem("None");
                    }
                    if (defencePrayer != e.getSource()) {
                        defencePrayer.setSelectedItem("None");
                    }
                }
            }
        };

        attackPrayer.addActionListener(specialPrayerListener);
        strengthPrayer.addActionListener(specialPrayerListener);
        defencePrayer.addActionListener(specialPrayerListener);

        ActionListener al = e -> new HighscoresRetreiver(rsnField.getText(), hitpointsSpinner, attackSpinner, strengthSpinner, defenceSpinner, rangedSpinner, magicSpinner, prayerSpinner);
        fetchLevelsButton.addActionListener(al);
        rsnField.addActionListener(al);

        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        if (args.length != 0) {
            switch (args[0]) {
                case "dbgen":
                    new OsrsDatabaseGenerator(args);
                    break;
                default:
                    new OsrsDpsCalculator(args);
                    break;
            }
        } else {
            new OsrsDpsCalculator(args);
        }

    }

    private void iterateThroughComponents(Container parent) {
        for (Component c : parent.getComponents()) {
            if (c instanceof JSpinner) {
                ((JSpinner) c).addChangeListener(e -> calculateAllNumbers());
                continue;
            }
            if (c instanceof JComboBox) {
                ((JComboBox) c).addActionListener(e -> calculateAllNumbers());
                continue;
            }
            if (c instanceof JCheckBox) {
                ((JCheckBox) c).addChangeListener(e -> calculateAllNumbers());
                continue;
            }
            if (c instanceof Container) {
                iterateThroughComponents((Container) c);
            }
        }
    }

    private static double roundResult(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void fillComboBoxes() throws IOException, URISyntaxException {
        for (NamedString str : OsrsDatabaseGenerator.urls) {
//            JSONObject items = new JSONObject(FileUtils.readFileToString(new File(dbDir, str.name + ".json"), StandardCharsets.UTF_8));
            JSONObject items = new JSONObject(FileUtils.readFileToString(new File(this.getClass().getResource(str.name + ".json").getPath()), Charset.defaultCharset()));
            ItemComboBoxModel itemComboBoxModel = new ItemComboBoxModel();
            for (String name : items.keySet()) {
                JSONArray array = items.getJSONArray(name);
                ArrayList<Object> list = new ArrayList<>(array.toList());
                Item i = new Item(name, list.toArray());
                itemComboBoxModel.addElement(i);
            }

            Item i = new EmptyItem();
            itemComboBoxModel.addElement(i);
            itemComboBoxModel.setSelectedItem(i);

            switch (str.name) {
                case "ammo":
                    ammoComboBox.setModel(itemComboBoxModel);
                    break;
                case "body":
                    chestComboBox.setModel(itemComboBoxModel);
                    break;
                case "feet":
                    bootsComboBox.setModel(itemComboBoxModel);
                    break;
                case "hand":
                    glovesComboBox.setModel(itemComboBoxModel);
                    break;
                case "head":
                    headComboBox.setModel(itemComboBoxModel);
                    break;
                case "legs":
                    legsComboBox.setModel(itemComboBoxModel);
                    break;
                case "neck":
                    amuletComboBox.setModel(itemComboBoxModel);
                    break;
                case "ring":
                    ringComboBox.setModel(itemComboBoxModel);
                    break;
                case "shield":
                    shieldComboBox.setModel(itemComboBoxModel);
                    break;
                case "twoh":
                    twohComboBox.setModel(itemComboBoxModel);
                    break;
                case "cape":
                    capeComboBox.setModel(itemComboBoxModel);
                    break;
                case "weapons":
                    weaponComboBox.setModel(itemComboBoxModel);
                    break;

            }
        }
    }

    private void calculateAllNumbers() {

        int effectiveLevel = 8;
        effectiveLevel += (int) strengthSpinner.getValue();

        switch ((String) Objects.requireNonNull(strengthPrayer.getSelectedItem())) {
            case "5%":
                effectiveLevel *= 1.05;
                break;
            case "10%":
                effectiveLevel *= 1.1;
                break;
            case "15%":
                effectiveLevel *= 1.15;
                break;
            case "Chivalry":
                effectiveLevel *= 1.18;
                break;
            case "Piety":
                effectiveLevel *= 1.23;
                break;
        }

        int combatLevel = (int) prayerSpinner.getValue();
        combatLevel += ((int) hitpointsSpinner.getValue() + (int) defenceSpinner.getValue()) / 4;



        int equipmentBonus = 0;

        for (Component c : equipmentSelectionPanel.getComponents()) {
            if (c instanceof JComboBox) {
                if (((JComboBox) c).getModel() instanceof ItemComboBoxModel) {
                    if (twoHandedCheckBox.isSelected()) {
                        if (c == weaponComboBox || c == shieldComboBox) continue;
                    } else {
                        if (c == twoHandedCheckBox) continue;
                    }
                    ItemComboBoxModel model = (ItemComboBoxModel) ((JComboBox) c).getModel();
                    Item i = (Item) model.getSelectedItem();
                    equipmentBonus += i.stats[Keywords.strength];
                }
            }
        }
        int hit = (int) (0.5 + effectiveLevel * (equipmentBonus + 64) / 640);
        maxHit.setText(Integer.toString(hit));

        Double dps;

        Item weapon;
        if (twoHandedCheckBox.isSelected()) {
            weapon = ((Item) twohComboBox.getSelectedItem());
        } else {
            weapon = ((Item) weaponComboBox.getSelectedItem());
        }
        int attackSpeed;
        if (!(weapon instanceof EmptyItem)) {
            attackSpeed = weapon.stats[Keywords.attackSpeed];
        } else {
            attackSpeed = 6;
        }
        double attackTime = (10 - attackSpeed) * 0.6;
        dps = (hit * 0.5) / attackTime;
        speedLabel.setText(Integer.toString(attackSpeed));
        dpsLabel.setText(Double.toString(roundResult(dps, 4)));
    }
}
