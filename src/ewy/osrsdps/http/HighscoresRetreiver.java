package ewy.osrsdps.http;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;

public class HighscoresRetreiver {

    public HighscoresRetreiver(String rsn, JSpinner hitpoints, JSpinner attack, JSpinner strength, JSpinner defence, JSpinner ranged, JSpinner magic, JSpinner prayer) {
        try {
            Document doc = Jsoup.parse(new URL("http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=" + rsn), 1000);
            Elements e = doc.getElementsByTag("table").get(1).getElementsByTag("tr");
            hitpoints.setValue(Integer.parseInt(e.get(8).getElementsByTag("td").get(3).getAllElements().first().text()));
            attack.setValue(Integer.parseInt(e.get(5).getElementsByTag("td").get(3).getAllElements().first().text()));
            strength.setValue(Integer.parseInt(e.get(7).getElementsByTag("td").get(3).getAllElements().first().text()));
            defence.setValue(Integer.parseInt(e.get(6).getElementsByTag("td").get(3).getAllElements().first().text()));
            ranged.setValue(Integer.parseInt(e.get(9).getElementsByTag("td").get(3).getAllElements().first().text()));
            magic.setValue(Integer.parseInt(e.get(11).getElementsByTag("td").get(3).getAllElements().first().text()));
            ranged.setValue(Integer.parseInt(e.get(10).getElementsByTag("td").get(3).getAllElements().first().text()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
