package ewy.osrsdps.http;

import ewy.osrsdps.OsrsDpsCalculator;
import ewy.osrsdps.util.NamedString;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class OsrsDatabaseGenerator {

    public static NamedString[] urls = {
            new NamedString("ammo", "http://oldschoolrunescape.wikia.com/wiki/Ammunition_slot_table"),
            new NamedString("body", "http://oldschoolrunescape.wikia.com/wiki/Body_slot_table"),
            new NamedString("cape", "http://oldschoolrunescape.wikia.com/wiki/Cape_slot_table"),
            new NamedString("feet", "http://oldschoolrunescape.wikia.com/wiki/Feet_slot_table"),
            new NamedString("hand", "http://oldschoolrunescape.wikia.com/wiki/Hand_slot_table"),
            new NamedString("head", "http://oldschoolrunescape.wikia.com/wiki/Head_slot_table"),
            new NamedString("legs", "http://oldschoolrunescape.wikia.com/wiki/Legwear_slot_table"),
            new NamedString("neck", "http://oldschoolrunescape.wikia.com/wiki/Neck_slot_table"),
            new NamedString("ring", "http://oldschoolrunescape.wikia.com/wiki/Ring_slot_table"),
            new NamedString("shield", "http://oldschoolrunescape.wikia.com/wiki/Shield_slot_table"),
            new NamedString("twoh", "http://oldschoolrunescape.wikia.com/wiki/Two-handed_slot_table"),
            new NamedString("cape", "http://oldschoolrunescape.wikia.com/wiki/Ammunition_slot_table"),
            new NamedString("weapons", "http://oldschoolrunescape.wikia.com/wiki/Weapons_table")};

    public OsrsDatabaseGenerator(String[] args) {
        try {
            //noinspection ResultOfMethodCallIgnored
            OsrsDpsCalculator.dbDir.mkdirs();
            for (NamedString str : urls) {
                generateFile(str);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            System.out.println("Error writing into user home directory.");
        }
    }

    private void generateFile(NamedString type) {
        JSONObject json = new JSONObject();
        Document soup;
        try {
            soup = Jsoup.parse(new URL(type.value), 5000);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading " + type.name + " table wiki page");
            return;
        }
        Element table = soup.body().getElementsByClass("wikitable").get(0);
        Elements rows = table.getElementsByTag("tr");
        rows.remove(0);
        for (Element item : rows) {
            ArrayList<Integer> statList = new ArrayList<>();
            int i = -1;
            for (Element stat : item.getElementsByTag("td")) {
                if (i == -1) {
                    i++;
                    continue;
                }
                int numberValue = 0;
                try {
                    numberValue = Integer.parseInt(stat.text());
                } catch (NumberFormatException e) {
                    //pass
                }
                statList.add(numberValue);
                i++;
            }
            json.put(item.getElementsByTag("td").first().text(), statList);
        }
        try {
            File file = new File(OsrsDpsCalculator.dbDir, type.name + ".json");
            if (file.exists()) {
                file.delete();
            }
            FileUtils.writeStringToFile(file, json.toString(4), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
