package ewy.osrsdps;

public interface Keywords {
    int stabAttack = 0;
    int slashAttack = 1;
    int crushAttack = 2;
    int magicAttack = 3;
    int rangedAttack = 4;
    int stabDefence = 5;
    int slashDefence = 6;
    int crushDefence = 7;
    int magicDefence = 8;
    int rangedDefence = 9;
    int strength = 10;
    int rangedStrength = 11;
    int magicDamage = 12;
    int prayerBonus = 13;
    int attackSpeed = 14;

    enum Types {
        MELEE, RANGED, MAGIC
    }
}
